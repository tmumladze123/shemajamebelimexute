package com.example.shemajamebelixuti

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shemajamebelixuti.databinding.FragmentMainBinding

class MainRVFragment : Fragment() {
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private val ourModel: OurViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding=FragmentMainBinding.inflate(inflater,container,false)
        ourModel.setValue()
        setAdapter()
        return binding.root
    }
    fun setAdapter()
    {
        var adapter=MainAdapter(ourModel.json.value!!)
        binding.rv.adapter=adapter
        binding.rv.layoutManager= LinearLayoutManager(context)
    }

}