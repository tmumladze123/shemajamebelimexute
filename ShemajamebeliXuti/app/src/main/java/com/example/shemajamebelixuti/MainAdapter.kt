package com.example.shemajamebelixuti

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.shemajamebelixuti.Json.Json
import com.example.shemajamebelixuti.databinding.RecycleritemBinding

class MainAdapter (var json: Json) :  RecyclerView.Adapter<MainAdapter.ViewHolder>(){

    inner class ViewHolder(var binding: RecycleritemBinding) : RecyclerView.ViewHolder(binding.root) {
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
            =ViewHolder(RecycleritemBinding.inflate(LayoutInflater.from(parent.context),parent,false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var adapter=ItemAdapter(json[position])
        holder.binding.rvItem.adapter=adapter
        holder.binding.rvItem.layoutManager= LinearLayoutManager(holder.binding.root.context)
    }

    override fun getItemCount(): Int =json.size

}

