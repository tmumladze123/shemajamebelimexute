package com.example.shemajamebelixuti

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shemajamebelixuti.Json.JsonSubList
import com.example.shemajamebelixuti.databinding.Item1Binding
import com.example.shemajamebelixuti.databinding.ItemBinding

class ItemAdapter(var json: JsonSubList) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var counter=0
    inner class ViewHolder(var binding: ItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.etItem.setHint(json[adapterPosition].hint)
        }
    }

    inner class ViewHolder1(var binding: Item1Binding) : RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            binding.field.setHint(json[adapterPosition].hint)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (json[position].field_type == inputType.CHOOSER.name)
            counter= 1
        else
            counter= 2
        return counter
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType==1) {
            ViewHolder1(Item1Binding.inflate(LayoutInflater.from(parent.context), parent, false))
        } else {
            ViewHolder(ItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (json[position].field_type == inputType.CHOOSER.name) {
            holder as ViewHolder1
            holder.onBind()
        } else {
            holder as ViewHolder
            holder.onBind()
        }

    }

    override fun getItemCount(): Int = json.size
}

